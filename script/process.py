# script/process.py

import os
import csv
import requests
import pandas as pd

#Constants

#source: https://fred.stlouisfed.org/series/TOTALNSA
URL_FRED_SALES_USA_NSA = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgc\
    olor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23ffff\
    ff&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12\
    &width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_to\
    oltip=yes&id=TOTALNSA&scale=left&cosd=1976-01-01&coed=2019-10-01&line_col\
    or=%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&\
    ost=-99999&oet=99999&mma=0&fml=a&fq=Monthly&fam=avg&fgst=lin&fgsnd=2009-0\
    6-01&line_index=1&transformation=lin&vintage_date=2019-11-08&revision_dat\
    e=2019-11-08&nd=1976-01-01'

#source: https://fred.stlouisfed.org/series/TOTALSA
URL_FRED_SALES_USA_SA = 'https://fred.stlouisfed.org/graph/fredgraph.csv?bgco\
    lor=%23e1e9f0&chart_type=line&drp=0&fo=open%20sans&graph_bgcolor=%23fffff\
    f&height=450&mode=fred&recession_bars=on&txtcolor=%23444444&ts=12&tts=12&\
    width=1168&nt=0&thu=0&trc=0&show_legend=yes&show_axis_titles=yes&show_too\
    ltip=yes&id=TOTALSA&scale=left&cosd=1976-01-01&coed=2019-10-01&line_color\
    =%234572a7&link_values=false&line_style=solid&mark_type=none&mw=3&lw=2&os\
    t=-99999&oet=99999&mma=0&fml=a&fq=Monthly&fam=avg&fgst=lin&fgsnd=2009-06-\
    01&line_index=1&transformation=lin&vintage_date=2019-11-08&revision_date=\
    2019-11-08&nd=1976-01-01'

#source: http://www.oica.net/category/sales-statistics/
URL_OICA_SALES_WORLD = ('http://www.oica.net/wp-content/uploads/total_sales_'+
    '2018.xlsx')

# directories
CACHE_DIR = 'cache/'
DATA_DIR = '../data/'

# local files
LOCAL_FRED_SALES_USA_NSA = CACHE_DIR + 'total_sales_usa_nsa.csv'
LOCAL_FRED_SALES_USA_SA = CACHE_DIR + 'total_sales_usa_sa.csv'
LOCAL_OICA_SALES_WORLD = CACHE_DIR + 'total_sales_world.xlsx' 

# output files

OUTPUT_USA_TOTAL_SALES = DATA_DIR + 'total_sales_usa_monthly.csv'
OUTPUT_WORLD_TOTAL_SALES = DATA_DIR + 'total_sales_world_annual.csv'

def prepare():

    if not os.path.exists(CACHE_DIR):
        os.makedirs(CACHE_DIR)

    if not os.path.exists(DATA_DIR):
        os.makedirs(DATA_DIR)

def retrieve():
    #Get the data from the url and cache it.
    
    # fred sales usa nsa - csv
    response = requests.get(URL_FRED_SALES_USA_NSA)        
    with open(LOCAL_FRED_SALES_USA_NSA, 'wb') as file:
        for chunk in response:
            file.write(chunk)
        file.close()

    # fred sales usa sa - csv
    response = requests.get(URL_FRED_SALES_USA_SA)        
    with open(LOCAL_FRED_SALES_USA_SA, 'wb') as file:
        for chunk in response:
            file.write(chunk)
        file.close()

    # oica sales world - xlsx
    response = requests.get(URL_OICA_SALES_WORLD)
    with open(LOCAL_OICA_SALES_WORLD, 'wb') as file:
        file.write(response.content)
        file.close()
            
def extract_and_transform():
    # load the csv, standardize ... tidy ...
    
    ########################### USA Sales ####################################
    ##########################################################################

    # defining strings
    merging_str = 'DATE'
    nsa_total_str = 'TOTALNSA'
    sa_total_str = 'TOTALSA'

    #reading files
    df_sales_nsa = pd.read_csv(LOCAL_FRED_SALES_USA_NSA)
    df_sales_sa = pd.read_csv(LOCAL_FRED_SALES_USA_SA)

    # merging total sales usa
    df_merged_usa_sales = df_sales_nsa.merge(df_sales_sa, on=merging_str)
    df_merged_usa_sales[nsa_total_str] = df_merged_usa_sales[nsa_total_str].round(decimals=2)
    df_merged_usa_sales[sa_total_str] = df_merged_usa_sales[sa_total_str].round(decimals=2)

    ########################### World Sales ##################################
    ##########################################################################

    # world total sales
    df_world_sales = pd.read_excel(LOCAL_OICA_SALES_WORLD)

    # dropping non-data lines
    data_starting_line = 4
    df_world_sales = df_world_sales.iloc[data_starting_line:,] 

    # dropping empty columns
    df_world_sales = df_world_sales.drop(columns=[df_world_sales.columns[1],
        df_world_sales.columns[2],df_world_sales.columns[3]])

    # Drop rows with any empty cells
    df_world_sales = df_world_sales.dropna(axis=0, how='any', thresh=None, 
        subset=None, inplace=False)

    # setting correct types
    for i in range(1,len(df_world_sales.columns)):
        column_name = df_world_sales.columns[i]
        df_world_sales[column_name] = df_world_sales[column_name].astype('int')

    # setting new header
    new_header = df_world_sales.iloc[0]
    df_world_sales.columns = new_header

    # dropping first line (header)
    data_starting_line = 1
    df_world_sales = df_world_sales.iloc[data_starting_line:,] 

    return df_merged_usa_sales, df_world_sales

def load(df_merged_usa_sales, df_world_sales):
    # save data frames in csv files
    
    df_merged_usa_sales.to_csv(OUTPUT_USA_TOTAL_SALES, index=False)
    df_world_sales.to_csv(OUTPUT_WORLD_TOTAL_SALES, index=False)

def run():
    prepare()
    retrieve()
    df_merged_usa_sales, df_world_sales = extract_and_transform()
    load(df_merged_usa_sales, df_world_sales)

if __name__ == '__main__':
    run()