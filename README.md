# World car sales statistics.

This data package contains time-series data from car sales worldwide. 

Automobile sales data represents total sales including all categories. The number of sales is in millions of units.

## Data

The data directory contains two files, one with US sales data and the other with worldwide sales data. The file, *total_sales_usa_monthly.csv*, contains monthly US car sales data from 1976 to 2019. Two measures for sales are presented, one [seasonal adjusted][Seasonal Adjustment] and the other not seasonal adjusted. Data were obtained from two different sources ([Seasonal Adjusted][FRED TOTALSA], [Not Seasonal Adjusted][FRED TOTALNSA]) from the FED St. Louis and combined into a single file.

The file, *total_sales_world_annual.csv*, contains annual car sales data by continent and by countries around the world. Data were obtained from the International Organization of Motor Vehicle Manufacturers - [OICA].

## Preparation

Inside the script directory, there is a file, ```process.py```, which extracts data from sources and creates the CSV files. 

Dependencies can be installed using the *requirements.txt* file within the directory.

```
pip install requirements.txt
```

The script can be run as:

```
python process.py
```

## License

This Data Package is made available under the Public Domain Dedication and License v1.0 whose full text can be found at: [PPDL]

[PPDL]: http://www.opendatacommons.org/licenses/pddl/1.0/
[Seasonal Adjustment]: https://en.wikipedia.org/wiki/Seasonal_adjustment
[FRED TOTALSA]: https://fred.stlouisfed.org/series/TOTALSA
[FRED TOTALNSA]: https://fred.stlouisfed.org/series/TOTALNSA
[OICA]: http://www.oica.net/category/sales-statistics/

